package org.eaves.video;

public class Receipt {

    private final int _value;

    public Receipt(int value) {
        _value = value;
    }

    public int value() {
        return _value;
    }

    public boolean successful() {
        return true;
    }
}
