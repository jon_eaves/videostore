package org.eaves.video;

public class Payment {
    private final int _value;

    public Payment(int value) {
        _value = value;
    }

    public int value() {
        return _value;
    }
}
