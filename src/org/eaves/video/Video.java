package org.eaves.video;

public class Video {

    private final int _cost;
    private final String _name;
    private boolean _available = true;

    public Video(String name, int costOfVideo) {
        _name = name;
        _cost = costOfVideo;
    }

    public int cost() {
        return _cost;
    }

    public String name() {
        return _name;
    }

    public boolean available() {
        return _available;
    }
    
    public boolean rent() {
        boolean thisRental = false;
        if (available()) {
            _available = false;
            thisRental = true;
        }
        return thisRental;
    }
}
