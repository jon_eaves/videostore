package org.eaves.video;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class VideoStore {

    private final Library _library;

    public VideoStore(Library library) {
        _library = library;
    }

    public List<Video> videos() {
        return _library.videoList();
    }

    public List<Video> searchFor(String result) {
        List<Video> rv = _library.videoList().stream().filter(v -> v.name().startsWith(result)).collect(Collectors.toList());
        return rv;
    }

    public void addVideo(Video v) {
        _library.add(v);
    }
}
