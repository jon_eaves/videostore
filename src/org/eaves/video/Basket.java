package org.eaves.video;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Basket {
    
    private Set<Video> _videos = new HashSet<Video>();
    
    public Basket(Customer c) {
    }

    public void rent(Video v) {
        if (v.rent()) {
            _videos.add(v);
        }
    }

    public Payment calculateOwing() {
        int cost = _videos.stream().map(Video::cost).mapToInt(Integer::intValue).sum();                
        return new Payment(cost);
    }

    public Receipt pay(Payment p) {
        return new Receipt(p.value());
    }

    public int size() {
        return _videos.size();
    }
}
