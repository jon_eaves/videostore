package org.eaves.video;

import java.util.List;

public interface Library {
    List<Video> videoList();

    void add(Video v);
}
