package org.eaves.video;

import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertTrue;

public class VideoStoreTest {
    
    @Test
    public void storeHasTwoVideosExactly() {
        Library library = new PreFilledStaticVideoLibrary(2, 3);
        
        VideoStore s = new VideoStore(library);
        List<Video> videos = s.videos();
        
        assertTrue(((List) videos).size() == 2);
        
    }
}
