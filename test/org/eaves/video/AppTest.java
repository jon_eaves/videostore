package org.eaves.video;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

import org.junit.Before;
import org.junit.Test;
import java.util.List;

public class AppTest {

    private VideoStore _videoStore;

    @Before
    public void setUp() throws Exception {
        _videoStore = getVideoStore();
    }

    private VideoStore getVideoStore() {
        Library library = new PreFilledStaticVideoLibrary(10, 5);
        return new VideoStore(library);
    }

    @Test
    public void createApplication() {
        VideoStore s = getVideoStore();
        Customer c = new Customer();
        
        List<Video> videos = s.videos();
        Video v = videos.get(0);

        Basket b = new Basket(c);
        b.rent(v);
        
        Payment p = b.calculateOwing();

        Receipt r = b.pay(p);
        
        assertEquals(r.value(), p.value());
        assertTrue(r.successful());
    }
    
    @Test
    public void searchVideoStore() {
        VideoStore s = getVideoStore();
        s.addVideo(new Video("Result", 5));
        
        SearchableStore ss = new SearchableStore(s);
        
        List<Video> videos = s.searchFor("Result");
        
        assertTrue(videos.size() > 0);
        
        videos = s.searchFor("NotFound");
        
        assertTrue(videos.size() == 0);
    }
    
    @Test
    public void rentingAVideo() {
        VideoStore s = getVideoStore();
        s.addVideo(new Video("Rentable", 5));

        SearchableStore ss = new SearchableStore(s);

        List<Video> videos = s.searchFor("Rentable");
        Video rentme = videos.get(0);
        
        assertTrue(rentme.available());
        
        Basket b = new Basket(new Customer());
        b.rent(rentme);
        
        assertFalse(rentme.available());

        videos = s.searchFor("Rentable");
        rentme = videos.get(0);

        assertFalse(rentme.available());

        videos = s.searchFor("NotFound");

        assertTrue(videos.size() == 0);
        
    }

}
