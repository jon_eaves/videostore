package org.eaves.video;

import java.util.ArrayList;
import java.util.List;

public class PreFilledStaticVideoLibrary implements Library {

    private final List<Video> _library = new ArrayList<>();
    
    public PreFilledStaticVideoLibrary(int sizeOfLibrary, int costOfVideo) {
        for (int i=0;i<sizeOfLibrary;i++) {
            add(new Video("Unknown", costOfVideo));
        }
    }

    @Override
    public List<Video> videoList() {
        return _library;
    }

    @Override
    public void add(Video v) {
        _library.add(v);
    }
}
