package org.eaves.video;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class BasketTest {
    
    @Test
    public void canAddVideoToBasket() {
        
        Customer c = new Customer();
        Basket b = new Basket(c);
        
        b.rent(new Video("Unknown", 5));

        assertEquals(1, b.size());
        
    }
    
    @Test
    public void newBasketIsAlwaysEmpty() {
        
        Customer c = new Customer();
        Basket b = new Basket(c);
        
        assertEquals(0, b.size());
    }
    
    @Test
    public void addingSameVideoTwiceOnlyAddsOnceIntoBasket() {
        
        Customer c = new Customer();
        Basket b = new Basket(c);
        
        Video v = new Video("Unknown", 5);
        b.rent(v);
        b.rent(v);
        
        assertEquals(1, b.size());
    }
    
    @Test
    public void addingDifferentVideosToBasket() {
        Customer c = new Customer();
        Basket b = new Basket(c);

        Video v1 = new Video("Unknown", 5);
        Video v2 = new Video("Unknown", 5);
        Video v3 = new Video("Unknown", 5);
        
        b.rent(v1);
        b.rent(v2);
        b.rent(v3);
        b.rent(v1);
        
        assertEquals(3, b.size());
        
    }

    @Test
    public void emptyBasketCreatesZeroValuePayment() {
        Customer c = new Customer();
        Basket b = new Basket(c);
        
        Payment p = b.calculateOwing();
        
        assertEquals(0, p.value());
        
    }
    
}
