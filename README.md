# videostore

The Ken Scambler DI talk that spawned the OO discussion about the implementation of the video store.

The key elements are a video store that you can search for titles, and rent the title including payment.
Ken's model was a typical "controller / services / storage" layered model and part of the very interesting
discussion was around the "coathanger" classes, in this case the "Rentals" - which was a hanger/container for a single
method.

The interesting part was about "what does a coathanger class mean about your design, and what would I do" - so I wrote this
to see/show what I would do.


